package com.epam.grow.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xotlanc
 */
public class TemperatureData implements Subject {
    private List<Observer> observers = new ArrayList<Observer>();
    int temperature = 12;

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        for(Observer observer : observers){
            observer.update(temperature);
        }
    }

    public int getState() {
        return temperature;
    }

    public void setState(int state) {
        temperature = state;
        notifyObservers();
    }


}

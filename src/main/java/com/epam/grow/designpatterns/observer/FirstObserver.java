package com.epam.grow.designpatterns.observer;

/**
 * Created by xotlanc
 */
public class FirstObserver implements Observer {
    public void update(int temperature) {
        System.out.println("first observer " + temperature);
    }
}

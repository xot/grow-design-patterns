package com.epam.grow.designpatterns.observer;

/**
 * Created by xotlanc
 */
public interface Subject {
    void registerObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();

    int getState();
    void setState(int state);

}

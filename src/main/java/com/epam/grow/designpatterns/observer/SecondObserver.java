package com.epam.grow.designpatterns.observer;

/**
 * Created by xotlanc
 */
public class SecondObserver implements Observer {
    public void update(int temperature) {
        System.out.println("second observer " + temperature);
    }
}

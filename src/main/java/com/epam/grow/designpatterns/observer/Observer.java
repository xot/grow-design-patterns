package com.epam.grow.designpatterns.observer;

/**
 * Created by xotlanc
 */
public interface Observer {

    void update(int temperature);
}


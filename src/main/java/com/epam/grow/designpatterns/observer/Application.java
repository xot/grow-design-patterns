package com.epam.grow.designpatterns.observer;

/**
 * Created by xotlanc
 */
public class Application {
    public static void main(String[] args) throws Exception {
        Subject temperatureData = new TemperatureData();

        Observer observer = new FirstObserver();
        Observer secondObserver = new SecondObserver();

        temperatureData.registerObserver(observer);

        temperatureData.setState(10);

        temperatureData.registerObserver(secondObserver);

        temperatureData.setState(194);

        temperatureData.removeObserver(observer);

        temperatureData.setState(14);

        temperatureData.removeObserver(observer);

    }
}

package com.epam.grow.designpatterns.builder;

/**
 * Created by xotlanc.
 */
public class Application {
    public static void main(String[] args) {
        new User.UserBuilder("john", "doe").age(10).phone("324").build();
    }
}

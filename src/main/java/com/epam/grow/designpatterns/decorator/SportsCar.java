package com.epam.grow.designpatterns.decorator;

/**
 * Created by xotlanc.
 */
public class SportsCar extends CarDecorator {

    public SportsCar(Car car) {
        super(car);
    }

    @Override
    public void assemble(){
        super.assemble();
        System.out.println("Adding more horsepower");
    }
}


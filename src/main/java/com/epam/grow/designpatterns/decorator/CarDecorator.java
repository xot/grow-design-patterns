package com.epam.grow.designpatterns.decorator;

/**
 * Created by xotlanc.
 */
public class CarDecorator implements Car {

    protected Car car;

    public CarDecorator(Car car){
        this.car = car;
    }

    @Override
    public void assemble() {
        this.car.assemble();
    }
}

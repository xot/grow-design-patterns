package com.epam.grow.designpatterns.decorator;

/**
 * Created by xotlanc.
 */
public class Application {

    public static void main(String[] args) throws Exception {
        Car sportsCar = new SportsCar(new BasicCar());
        sportsCar.assemble();
        System.out.println("");
        Car sportsLuxuryCar = new SportsCar(new LuxuryCar(new BasicCar()));
        sportsLuxuryCar.assemble();

    }
}

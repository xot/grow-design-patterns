package com.epam.grow.designpatterns.decorator;

/**
 * Created by xotlanc.
 */
public class LuxuryCar extends CarDecorator {

    public LuxuryCar(Car car) {
        super(car);
    }

    @Override
    public void assemble(){
        super.assemble();
        System.out.println("Adding leather seats");
    }
}

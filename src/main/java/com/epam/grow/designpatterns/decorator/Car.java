package com.epam.grow.designpatterns.decorator;

/**
 * Created by xotlanc.
 */
public interface Car {
    void assemble();
}

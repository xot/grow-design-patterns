package com.epam.grow.designpatterns.command;

/**
 * Created by xotlanc
 */
public interface Order {
    void execute();
}

package com.epam.grow.designpatterns.command;

/**
 * Created by xotlanc
 */
public class Application {

    public static void main(String[] args) throws Exception{
        Stock stock = new Stock("name", 21);

        BuyStock buyStock = new BuyStock(stock);
        SellStock sellStock = new SellStock(stock);

        Broker broker = new Broker();
        broker.takeOrder(buyStock);
        broker.takeOrder(sellStock);

        broker.placeOrders();
    }
}

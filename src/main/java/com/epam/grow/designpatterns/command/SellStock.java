package com.epam.grow.designpatterns.command;

/**
 * Created by xotlanc
 */
public class SellStock implements Order {
    private Stock stock;

    public SellStock(Stock stock){
        this.stock = stock;
    }

    @Override
    public void execute() {
        stock.sell();
    }
}

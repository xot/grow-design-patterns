package com.epam.grow.designpatterns.command;

/**
 * Created by xotlanc
 */
public class Stock {
    private final String name;
    private final int quantity;

    public Stock(String name, int quantity){
        this.name = name;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public void buy(){
        System.out.println("Stock name: " + name + ", quantity: " + quantity + " bought");
    }

    public void sell(){
        System.out.println("Stock name: " + name + ", quantity: " + quantity + " sold");
    }

}

package com.epam.grow.designpatterns.bridge;

/**
 * Created by xotlanc.
 */
public class DrawingApiFirstImpl implements DrawingApi {

    @Override
    public void drawCircle(double x, double y, double radius) {
        System.out.printf("API1.circle at %f:%f radius %f\n", x, y, radius);
    }
}

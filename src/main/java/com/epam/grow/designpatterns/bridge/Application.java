package com.epam.grow.designpatterns.bridge;

/**
 * Created by xotlanc.
 */
public class Application {

    public static void main(String[] args) throws Exception {

        Shape[] shapes = new Shape[] {
                new CircleShape(1, 2, 3, new DrawingApiFirstImpl()),
                new CircleShape(5, 7, 11, new DrawingApiSecondImpl())
        };

        for (Shape shape : shapes) {
            shape.resizeByPercentage(2.5);
            shape.draw();
        }
    }
}

package com.epam.grow.designpatterns.bridge;

/**
 * Created by xotlanc.
 */
public abstract class Shape {
    protected DrawingApi drawingAPI;

    protected Shape(DrawingApi drawingAPI){
        this.drawingAPI = drawingAPI;
    }

    public abstract void draw();
    public abstract void resizeByPercentage(double pct);
}

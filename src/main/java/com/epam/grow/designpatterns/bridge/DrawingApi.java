package com.epam.grow.designpatterns.bridge;

/**
 * Created by xotlanc.
 */
public interface DrawingApi {
    void drawCircle(double x, double y, double radius);
}

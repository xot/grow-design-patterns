package com.epam.grow.designpatterns.flyweight;

/**
 * Created by xotlanc.
 */
public interface Platform {
    void execute(Code code);
}

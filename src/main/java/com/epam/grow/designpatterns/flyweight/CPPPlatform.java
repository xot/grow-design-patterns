package com.epam.grow.designpatterns.flyweight;

import javafx.application.*;

/**
 * Created by xotlanc.
 */
public class CPPPlatform implements Platform {
    public CPPPlatform(){
        System.out.println("CPPPlatform object created");
    }

    @Override
    public void execute(Code code) {
        System.out.println("Compiling and executing CPP code");
    }
}

package com.epam.grow.designpatterns.flyweight;

/**
 * Created by xotlanc.
 */
public class Code {
    private String code;

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        this.code = code;
    }
}

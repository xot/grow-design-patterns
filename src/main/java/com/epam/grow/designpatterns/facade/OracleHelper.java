package com.epam.grow.designpatterns.facade;

/**
 * Created by xotlanc.
 */
public class OracleHelper {

    public static String getOracleDBConnection(){
        System.out.println("getting Oracle connection");
        return null;
    }

    public void generateOraclePDFReport(String tableName, String con){
        System.out.println("generating pdf report from Oracle table");
    }

    public void generateOracleHTMLReport(String tableName, String connection){
        System.out.println("generating html report from html table");
    }
}

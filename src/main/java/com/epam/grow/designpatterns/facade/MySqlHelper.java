package com.epam.grow.designpatterns.facade;

/**
 * Created by xotlanc.
 */
public class MySqlHelper {
    public static String getMySqlDBConnection(){
        System.out.println("getting mysql connection");
        return null;
    }

    public void generateMySqlPDFReport(String tableName, String con){
        System.out.println("generating pdf report from mysql table");
    }

    public void generateMySqlHTMLReport(String tableName, String connection){
        System.out.println("generating html report from html table");
    }
}


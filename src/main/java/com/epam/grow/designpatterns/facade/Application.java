package com.epam.grow.designpatterns.facade;

/**
 * Created by xotlanc.
 */
public class Application {

    public static void main(String[] args) throws Exception {
        String tableName="Employee";

        //generating MySql HTML report and Oracle PDF report without using Facade
        String con = MySqlHelper.getMySqlDBConnection();
        MySqlHelper mySqlHelper = new MySqlHelper();
        mySqlHelper.generateMySqlHTMLReport(tableName, con);

        String con1 = OracleHelper.getOracleDBConnection();
        OracleHelper oracleHelper = new OracleHelper();
        oracleHelper.generateOraclePDFReport(tableName, con1);

        //generating MySql HTML report and Oracle PDF report using Facade
        FacadeGenerator.generateReport(FacadeGenerator.DBTypes.MYSQL, FacadeGenerator.ReportTypes.HTML, tableName);
        FacadeGenerator.generateReport(FacadeGenerator.DBTypes.ORACLE, FacadeGenerator.ReportTypes.PDF, tableName);

    }
}

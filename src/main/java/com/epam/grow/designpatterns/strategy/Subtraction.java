package com.epam.grow.designpatterns.strategy;

/**
 * Created by xotlanc.
 */
public class Subtraction implements Operation {
    public double doOperation(int firstArg, int secondArg) {
        return firstArg - secondArg;
    }
}

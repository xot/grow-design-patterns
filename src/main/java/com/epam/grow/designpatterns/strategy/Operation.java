package com.epam.grow.designpatterns.strategy;

/**
 * Created by xotlanc.
 */
public interface Operation {
    double doOperation(int firstArg, int secondArg);
}

package com.epam.grow.designpatterns.strategy;

/**
 * Created by xotlanc.
 */
public class Calculation {
    Operation operation;

    public Calculation(Operation operation){
        this.operation = operation;
    }

    public double performCalculation(int firstArg, int secondArg){
        return operation.doOperation(firstArg, secondArg);
    }
}

package com.epam.grow.designpatterns.strategy;

/**
 * Created by xotlanc.
 */
public class Application {
    public static void main(String[] args) throws Exception {
        Calculation addition = new Calculation(new Addition());
        Calculation sub = new Calculation(new Subtraction());

        System.out.println(addition.performCalculation(3, 14));
        System.out.println(sub.performCalculation(3, 14));

    }
}

package com.epam.grow.designpatterns.composite;

/**
 * Created by xotlanc.
 */
public interface AbstractFile {
    void ls();
}

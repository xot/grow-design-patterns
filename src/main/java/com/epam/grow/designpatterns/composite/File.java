package com.epam.grow.designpatterns.composite;

/**
 * Created by xotlanc.
 */
public class File implements AbstractFile {

    private String fileName;

    public File(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void ls() {
        System.out.println("print ls for file: " + fileName);
    }
}

package com.epam.grow.designpatterns.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xotlanc.
 */
public class Directory implements AbstractFile {
    private List<AbstractFile> directory = new ArrayList<>();
    private String name;

    public Directory(String name) {
        this.name = name;
    }

    @Override
    public void ls() {
        System.out.println("printls for directory " + name);
        directory.forEach(AbstractFile::ls);
    }

    public void add(AbstractFile abstractFile){
        directory.add(abstractFile);
    }

    public void remove(AbstractFile abstractFile){
        directory.remove(abstractFile);
    }

}

package com.epam.grow.designpatterns.factory;

/**
 * Created by xotlanc.
 */
public class Application {
    public static void main(String[] args) throws Exception {
        ShapeFactory shapeFactory = new ShapeFactory();

        Shape shape1 = shapeFactory.getShape("rectangle");
        shape1.draw();

        Shape shape2 = shapeFactory.getShape("square");
        shape2.draw();
    }
}

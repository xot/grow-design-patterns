package com.epam.grow.designpatterns.factory;

/**
 * Created by xotlanc.
 */
public interface Shape {
    void draw();
}

package com.epam.grow.designpatterns.factory;

/**
 * Created by xotlanc.
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("draw rectangle");
    }
}

package com.epam.grow.designpatterns.factory;

/**
 * Created by xotlanc.
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("draw circle");
    }
}

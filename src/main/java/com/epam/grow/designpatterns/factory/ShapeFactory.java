package com.epam.grow.designpatterns.factory;

import sun.security.provider.SHA;

/**
 * Created by xotlanc.
 */
public class ShapeFactory {

    public Shape getShape(String shapeType){
        if(null == shapeType){
            return null;
        }
        if(shapeType.equalsIgnoreCase("circle")){
            return new Circle();
        }
        if(shapeType.equalsIgnoreCase("rectangle")){
            return new Rectangle();
        }
        if(shapeType.equalsIgnoreCase("square")){
            return new Square();
        }

        return null;
    }
}


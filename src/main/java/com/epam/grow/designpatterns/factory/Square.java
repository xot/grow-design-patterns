package com.epam.grow.designpatterns.factory;

import java.awt.*;

/**
 * Created by xotlanc.
 */
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("draw square");
    }
}

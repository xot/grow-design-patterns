package com.epam.grow.designpatterns.proxy;

/**
 * Created by xotlanc.
 */
public interface Image {
    void displayImage();
}

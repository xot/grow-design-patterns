package com.epam.grow.designpatterns.adapter;

/**
 * Created by xotlanc.
 */
public class Socket {
    public Volt getVolt(){
        return new Volt(120);
    }
}

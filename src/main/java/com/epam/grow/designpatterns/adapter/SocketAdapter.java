package com.epam.grow.designpatterns.adapter;

/**
 * Created by xotlanc.
 */
public interface SocketAdapter {
    Volt get120Volt();
    Volt get12Volt();
    Volt get3Volt();
}

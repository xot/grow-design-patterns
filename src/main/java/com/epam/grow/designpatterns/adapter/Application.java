package com.epam.grow.designpatterns.adapter;

/**
 * Created by xotlanc
 */
public class Application {
    public static void main(String[] args) throws Exception {

        SocketAdapter socketAdapter = new SocketAdapterImpl();
        Volt v3 = socketAdapter.get3Volt();
        Volt v12 = socketAdapter.get12Volt();
        Volt v120 = socketAdapter.get120Volt();
        System.out.println("v3 volts using Object Adapter="+v3.getVolts());
        System.out.println("v12 volts using Object Adapter="+v12.getVolts());
        System.out.println("v120 volts using Object Adapter="+v120.getVolts());
    }
}
